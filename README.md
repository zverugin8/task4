# Task: 

**Deploy runners on stage compute instances, install node.js dependencies, lint and test code.**

## Directory stucture

```

shell_scripts - Initialization scripts for runners
src           - Node source code
tf            - Terraform files for runners

```
## What i did:
1. Wrote Terraform files for spot runners.
2. Installed Standard linter for Node.js.
3. Added lint step to the test stage.


### Getting Started with Create React App 

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.


